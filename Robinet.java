public class Robinet extends Reservoir
{
    private static final double CAPACITE_INFINIE = Double.POSITIVE_INFINITY;
    private static Eau e=new Eau();
    public Robinet()
    {
        super(e,CAPACITE_INFINIE);
    }
}

