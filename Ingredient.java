public abstract class Ingredient
{
    private String nom;

    protected Ingredient(String nom)
    {
        this.nom=nom;
    }
    public String toString()
    {
        return this.nom;
    }
    public boolean equals(Object obj)
    {
        if(this==obj)
            return true;
        if(obj==null || getClass()!=obj.getClass())
            return false;

        Ingredient temp= (Ingredient) obj;
        if(this.nom==null)
            return temp.nom==null;
            else
                return this.nom.equals(temp.nom);
    }
}