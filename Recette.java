public class Recette {
    private Ingredient[] mesIngredients;
    private double[] mesQuantites;
    private double prix;
    private String nom;

    public Recette(String nom, double prix, Ingredient[] mI, double[] mQ) {
        this.nom = nom;
        this.prix = prix;
        this.mesIngredients = mI;
        this.mesQuantites = mQ;
    }

    // Accesseur pour obtenir le tableau d'ingrédients
    public Ingredient[] getMesIngredients() {
        return mesIngredients;
    }

    // Accesseur pour obtenir le prix de la recette
    public double getPrix() {
        return prix;
    }

    // Accesseur pour obtenir le nom de la recette
    public String getNom() {
        return nom;
    }
}
