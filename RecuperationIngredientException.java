public class RecuperationIngredientException extends Exception {

    // Constructeur avec un unique argument String message
    public RecuperationIngredientException(String message) {
        super(message);
    }

    // Méthode getMessage() pour obtenir le message d'erreur
    @Override
    public String getMessage() {
        return super.getMessage(); // Retourne le message d'erreur
    }
}
