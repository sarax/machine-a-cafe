
public class Reservoir
{
    
    private Ingredient unIngredient;
    private double capacite;
    private double niveau;
    public Reservoir(Ingredient i, double capacite)
    {
        this.unIngredient= i;
        this.capacite=capacite;
        this.niveau=capacite;
    }
    public Ingredient getIngredient()
    {
        return this.unIngredient;
    }
    public void remplir()
    {
        this.niveau=capacite;
    }
    
    public void recuperer(double q) throws RecuperationIngredientException
    {
        if (this.niveau < q) {
            // Levée de l'exception si la quantité demandée dépasse la quantité disponible
            throw new RecuperationIngredientException("Quantité insuffisante disponible : " + this.unIngredient + ", Quantité disponible : " + this.niveau);
        }
        this.niveau=niveau-q;
    }
}