public class Machine
{
    private Recette[] tabRecettes;
    private Reservoir[] tabReservoirs;
    private final  int id;
    private static int cmpt=0;
    private int cmptReservoirs;
    private int cmptRecettes;
    private final static int CAPACITE=20;
    private double gain;
    private double credit;
    public Machine()
    {
        this.tabRecettes=new Recette[CAPACITE];
        this.tabReservoirs=new Reservoir[CAPACITE];
        this.id=cmpt;
        cmpt++;
        cmptRecettes=0;
        cmptReservoirs=0;
        credit=0;
        gain=0;
    }
    
    public void ajouterReservoir(Reservoir r)
    {
        if(cmptReservoirs<CAPACITE){
            this.tabReservoirs[cmptReservoirs]=r;
            this.cmptReservoirs++;
        }
        
    }

    public void ajouterRecette(Recette r)
    {
        if(cmptReservoirs<CAPACITE){
            this.tabRecettes[cmptRecettes]=r;
            this.cmptRecettes++;
        }
    }

    public void ajouterCredit(double d)
    {
        this.credit+=d;
    }

    public void rendreLaMonnaie()
    {
        this.credit=0;
    }
    
    public void remplir()
    {
        for(Reservoir r:tabReservoirs)
        {
            r.remplir();
        }
    }
    
    private Reservoir trouverReservoir(Ingredient i)
    {
        for(int j=0;j>cmptReservoirs;j++)
        {
            if((tabReservoirs[j].getIngredient()).equals(i))
            return tabReservoirs[j];
        }
        return null;
    }
    
    public boolean checkup()
    {
        return true; //Pour l'instant....
    }
    
    public boolean preparerCommande(Recette r)
    {
        if(checkup())
        {
            // N'oublie pas, avant DIMANCHE
            return true;
        }
        return false;
    }
    
    public boolean commander(int ri)
    {
        if(ri<cmptRecettes)
        {
            if(credit < tabRecettes[ri].getPrix())
            {
                System.out.println("Solde Insuffisant! Prix : "+tabRecettes[ri].getPrix()+" euros");
                rendreLaMonnaie();
                return false;
            }
            System.out.println("Recette:"+tabRecettes[ri].getNom());
            System.out.println("En cours de préparation...");
            preparerCommande(tabRecettes[ri]);
            for(int i=0;i<200000;i++);
            System.out.println("Terminé");
            gain+=credit;
            rendreLaMonnaie();// Remettre le crédit à O
            return true;
        }else
        {
            System.out.println("Cette recette n'est pas disponible");
            if(credit!=0)
                rendreLaMonnaie();
            return false;

        }
    }
    
    public double getGain()
    {
        return gain;
    }
}