public class TestMachineACafe
{
    public static void main(String[] args)
    {
        Ingredient[] desIngredients = { new Lait(), new Cafe(), new Eau() };
        double[] quantitesDesIngredients={ 3.00, 1.00, 2.00};
        Reservoir[] desReservoirs={new Reservoir(desIngredients[0],20), new Reservoir(desIngredients[0],20), new Reservoir(desIngredients[0],20)};

        Recette CafeAuLait=new Recette("Café au lait", 2.00, desIngredients,quantitesDesIngredients);
        
        Machine laMachineDuCrous=new Machine();
        laMachineDuCrous.ajouterRecette(CafeAuLait);

        for(Reservoir r: desReservoirs)
        laMachineDuCrous.ajouterReservoir(r);

        laMachineDuCrous.ajouterCredit(2);
        laMachineDuCrous.commander(0);

        System.out.println(laMachineDuCrous.getGain());

    }
}